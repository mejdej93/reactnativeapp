import React from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';
import {colors} from "../../styles/styles";

export function LoadingSpinner(props) {

    const styles = StyleSheet.create({
        spinner: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });

    return (
        <View style={styles.spinner}>
            <ActivityIndicator
                size="large"
                color={colors.primary}
            />
        </View>
    )
}