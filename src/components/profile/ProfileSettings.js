import React, {Component} from 'react';
import {Image, TouchableOpacity, StyleSheet} from 'react-native';
import settingsIcon from './../../assets/settings.png';

export class ProfileSettings extends Component {
    styles = StyleSheet.create({
        image: {
            height: 30,
            width: 30,
            padding: 10,
            marginRight: 10
        }
    });

    constructor(props) {
        super(props);
        this.state = {
            profilePhotoPath: null
        }
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
            >
                <Image
                    style={this.styles.image}
                    source={settingsIcon}
                />
            </TouchableOpacity>
        );
    }
}