import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {customStyles} from "../../styles/styles";
import {Text, DatePickerAndroid} from 'react-native';

export class DatePicker extends Component {

    static propTypes = {
        onSelect: PropTypes.func.isRequired,
        value: PropTypes.number
    };

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(this.props.value).toString() || null
        };
    }

    openDatePicker = async () => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: this.state.date || new Date(year, month, day)
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                const d = new Date(year, month + 1, day + 1);
                const date = `${d.getDay()}-${d.getMonth()}-${d.getFullYear()}`;

                this.setState({
                    date
                });
                this.props.onSelect(date);
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    };

    render() {
        return (
            <Fragment>
                <Text style={customStyles.label}>{this.props.label}</Text>
                <Text
                    style={customStyles.textUnderline}
                    value={this.state.date}
                    onPress={this.openDatePicker}>
                    {this.state.date}
                </Text>
            </Fragment>
        );
    }
}