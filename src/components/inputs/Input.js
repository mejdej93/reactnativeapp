import React, {Component, Fragment} from 'react';
import {Text, TextInput} from 'react-native';
import PropTypes from 'prop-types';
import {customStyles, colors} from "../../styles/styles";

export class Input extends Component {

    static propTypes = {
        onChange: PropTypes.func.isRequired,
        label: PropTypes.string,
        keyboardType: PropTypes.string
    };

    static defaultProps = {
        keyboardType: 'default'
    };

    onChange = val => {
        this.props.onChange(val);
    };

    render() {
        return (
            <Fragment>
                <Text style={customStyles.label}>{this.props.label}</Text>
                <TextInput
                    name="input"
                    style={customStyles.input}
                    onChangeText={this.onChange}
                    keyboardType={this.props.keyboardType}
                    underlineColorAndroid={colors.primary}
                />
            </Fragment>
        )
    }
}