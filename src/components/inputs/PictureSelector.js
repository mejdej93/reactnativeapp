import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, CameraRoll} from 'react-native';
import PropTypes from 'prop-types';
import {customStyles} from "../../styles/styles";

export class PictureSelector extends Component {

    styles = StyleSheet.create({
        cameraRoll: {
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'space-around',
            flexDirection: 'row',
            flexWrap: 'wrap'
        },
        thumbnail: {
            marginTop: 10,
            width: 100,
            height: 100
        }
    });

    static propTypes = {
        onSelect: PropTypes.func.isRequired,
        label: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {
            photos: []
        };
    }

    getPhotos = () => {
        CameraRoll.getPhotos(
            {
                first: this.props.max,
                assetType: 'All'
            })
            .then(photos => this.setState(
                {
                    photos: photos.edges
                })
            );
    };

    selectImage = imageIndex => {
        const photos = this.state.photos;
        this.setState({
            selectedImage: photos[imageIndex]
        });

        this.props.onSelect(photos[imageIndex]);
    };

    componentDidMount() {
        this.getPhotos();
    }

    render() {
        return (
            <Fragment>
                <Text style={customStyles.label}>{this.props.label}</Text>
                <ScrollView
                    contentContainerStyle={this.styles.cameraRoll}>
                    {this.state.photos.map((p, i) => {
                        return (
                            <TouchableOpacity
                                onPress={() => this.selectImage(i)}
                                key={i}>
                                <Image
                                    key={i}
                                    style={StyleSheet.flatten(
                                        [
                                            this.styles.thumbnail,
                                            {
                                                borderRadius: p === this.state.selectedImage ? 50 : 0
                                            }
                                        ])}
                                    source={{uri: p.node.image.uri}}
                                />
                            </TouchableOpacity>
                        );
                    })}
                </ScrollView>
            </Fragment>
        );
    }
}