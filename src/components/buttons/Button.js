import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {customStyles, colors} from "../../styles/styles";

export class Button extends Component {

    static propTypes = {
        onPress: PropTypes.func.isRequired,
        label: PropTypes.string.isRequired,
        disabled: PropTypes.bool,
        type: PropTypes.oneOf([
            'success', 'error'
        ])
    };

    static defaultProps = {
        disabled: false,
        type: 'success'
    };

    onPress = () => {
        if (!this.props.disabled) {
            this.props.onPress();
        }
    };

    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={StyleSheet.flatten([customStyles.button, {
                    backgroundColor: this.props.type === 'error' ? colors.error : colors.primary
                }])}
                onPress={() => this.onPress()}
            >
                <Text style={customStyles.buttonText}>{this.props.label}</Text>
            </TouchableOpacity>
        );
    }
}