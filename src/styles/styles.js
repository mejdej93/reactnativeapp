import {StyleSheet} from 'react-native';

export const colors = {
    primary: '#1ab394',
    error: '#ed5565',
    tabs: '#415463',
    fontColor: 'white',
    gray: 'silver',
    background: '#293846',
    borderBottom: '#212d38'
};

export const sizes = {
    inputHeight: 40
};

export const customStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: colors.background,
        padding: 20
    },
    containerSpaceBetween: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    image: {
        width: '100%',
        maxHeight: 200
    },
    inputContainer: {
        marginBottom: 10
    },
    input: {
        color: colors.fontColor,
        height: sizes.inputHeight
    },
    slider: {
        height: sizes.inputHeight
    },
    picker: {
        color: colors.fontColor
    },
    switchContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    switchLabel: {
        textAlign: 'left',
        color: colors.fontColor,
        fontSize: 18
    },
    switch: {
        flexGrow: 1
    },
    pickerItem: {
        fontSize: 24,
        textAlign: 'center'
    },
    button: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
        marginTop: 15,
        paddingLeft: 10,
        paddingRight: 10,
        height: 50
    },
    buttonText: {
        color: colors.fontColor,
        fontSize: 18
    },
    text: {
        color: colors.fontColor
    },
    textBold: {
        color: colors.fontColor,
        fontWeight: '600',
        fontSize: 18
    },
    textJustified: {
        textAlign: 'justify'
    },
    textUnderline: {
        textAlignVertical: 'bottom',
        height: sizes.inputHeight - 10,
        color: colors.fontColor,
        marginTop: 0,
        paddingBottom: 3,
        borderBottomWidth: 1,
        borderBottomColor: colors.primary
    },
    label: {
        color: colors.fontColor,
        fontSize: 18,
        marginBottom: 5
    },
    list: {
        display: 'flex'
    },
    listElement: {
        display: 'flex',
        flexDirection: 'row',
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 40
    }
});