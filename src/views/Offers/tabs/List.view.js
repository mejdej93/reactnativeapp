import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {OffersList} from "./list/OffersList";
import {customStyles} from "../../../styles/styles";

export class ListView extends Component {

    state = {
        offers: []
    };

    styles = StyleSheet.create({
        summaryLabel: {
            display: 'flex',
            alignItems: 'flex-end',
            marginTop: 15,
        },
        summaryContainer: {
            marginTop: 15,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end'
        },
        summary: StyleSheet.flatten([customStyles.textBold, {
            fontSize: 24,
            textAlign: 'left'
        }]),
        expenseInfo: {
            display: 'flex',
            flexDirection: 'column'
        },
        viewPager: {
            flex: 1
        }
    });

    componentDidMount() {
        this.fetchOffers();
    }

    onOfferSelection = item => {
        this.props.navigation.navigate('DetailsView', {item});
    };

    fetchOffers = () => {
        fetch('http://192.168.1.6:3000')
            .then(response => {
                return response.json();
            })
            .then((offers) => {
                this.setState({
                    offers
                });
            });
    };

    render() {
        const {
            offers
        } = this.state;

        return (
            <View style={customStyles.container}>

                <OffersList
                    items={offers}
                    onPress={this.onOfferSelection}
                />

                <View style={this.styles.summaryContainer}>
                    <View style={this.styles.summaryLabel}>
                        <Text style={customStyles.text}>
                            Liczba ofert:
                        </Text>
                        <Text style={this.styles.summary}>
                            {offers.length}
                        </Text>
                    </View>
                </View>

            </View>
        );
    }
}
