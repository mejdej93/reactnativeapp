import React, {Component, Fragment} from 'react';
import {StyleSheet, View, Text, AsyncStorage} from 'react-native';
import {Button} from "../../../components/buttons/Button";
import {OffersList} from "./list/OffersList";
import {LoadingSpinner} from "../../../components/utils/LoadingSpinner";
import {customStyles} from "../../../styles/styles";

export class FavouritesView extends Component {

    styles = StyleSheet.create({
        noDataInfoContainer: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        },
        noDataInfo: StyleSheet.flatten([customStyles.textBold, customStyles.label, {
            fontSize: 24
        }])
    });

    constructor(props) {
        super(props);
        this.state = {
            favourites: [],
            loading: true
        }
    }

    componentDidMount = () => {
        this.focusSubscription = this.props.navigation.addListener(
            'didFocus',
            () => {
                this.loadFavourites();
            }
        );
    };

    componentWillUnmount() {
        this.focusSubscription.remove();
    }

    loadFavourites = () => {
        try {
            const favourites = [];
            AsyncStorage.getAllKeys((err, keys) => {
                AsyncStorage.multiGet(keys, (err, stores) => {
                    if (stores.length) {
                        stores.map((result, i, store) => {
                            favourites.push(JSON.parse(store[i][1]));
                        });
                    }
                    this.setState({
                        favourites,
                        loading: false
                    });
                });
            });
        } catch (err) {

        }
    };

    clearStorage = () => {
        AsyncStorage.clear();
        this.setState({
            favourites: []
        });
    };

    onOfferSelection = item => {
        this.props.navigation.navigate('DetailsView', {item, favourite: true});
    };

    renderListOrMessage() {
        if (this.state.favourites.length) {
            return (
                <Fragment>

                    <OffersList
                        items={this.state.favourites}
                        onPress={this.onOfferSelection}
                    />

                    <Button
                        label="Usuń wszystkie"
                        type="error"
                        onPress={this.clearStorage}
                    />
                </Fragment>
            )
        } else {
            return (
                <View style={this.styles.noDataInfoContainer}>
                    <Text style={this.styles.noDataInfo}>Lista jest pusta</Text>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={[customStyles.container, customStyles.containerSpaceBetween]}>
                {
                    this.state.loading ? <LoadingSpinner/> : this.renderListOrMessage()
                }
            </View>
        );
    }
}
