import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import {colors, customStyles} from "../../../../styles/styles";

export function OffersListElement({title, value, date, onPress}) {

    const styles = StyleSheet.create({
        expenseInfo: {
            display: 'flex',
            flexDirection: 'column'
        },
        bordered: {
            borderBottomWidth: 1,
            borderBottomColor: colors.borderBottom
        },
        rowHeight: {
            height: 60
        }
    });

    return (
        <TouchableOpacity
            style={[customStyles.listElement, styles.bordered, styles.rowHeight]}
            key={title}
            onPress={() => onPress()}>
            <View style={styles.expenseInfo}>
                <Text style={customStyles.text}>
                    {title}
                </Text>
                <Text style={customStyles.text}>
                    {date}
                </Text>
            </View>
            <Text style={customStyles.textBold}>
                {value}
            </Text>
        </TouchableOpacity>
    );
}