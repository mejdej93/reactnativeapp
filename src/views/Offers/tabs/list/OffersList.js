import React, {Component} from 'react';
import {FlatList} from 'react-native';
import PropTypes from 'prop-types';
import {OffersListElement} from "./OffersListElement";
import {customStyles} from "../../../../styles/styles";

export class OffersList extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired,
        onPress: PropTypes.func
    };

    _keyExtractor = (item, index) => index.toString();

    render() {
        return (
            <FlatList
                style={customStyles.list}
                data={this.props.items}
                keyExtractor={this._keyExtractor}
                renderItem={({item}) => (
                    <OffersListElement
                        key={item.title}
                        title={item.title}
                        date={item.date}
                        value={item.value}
                        onPress={() => this.props.onPress(item)}
                    />
                )
                }
            />
        );
    }
}