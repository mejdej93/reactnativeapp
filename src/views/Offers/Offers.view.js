import React from 'react';
import {ListView} from "./tabs/List.view";
import {FavouritesView} from "./tabs/Favourites.view";
import {createMaterialTopTabNavigator} from 'react-navigation';
import {colors} from "../../styles/styles";

export const OffersView = createMaterialTopTabNavigator(
    {
        List: {
            screen: ListView,
            navigationOptions: {
                title: 'Oferty pracy'
            }
        },
        Favourite: {
            screen: FavouritesView,
            navigationOptions: {
                title: 'Ulubione'
            }
        }
    },
    {
        tabBarOptions: {
            indicatorStyle: {
                borderBottomWidth: 2,
                borderBottomColor: colors.borderBottom
            },
            pressColor: colors.primary,
            style: {
                backgroundColor: colors.tabs
            }
        }
    }
);