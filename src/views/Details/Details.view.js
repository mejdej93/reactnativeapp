import React, {Component} from 'react';
import {
    ToastAndroid,
    View,
    ScrollView,
    Text,
    AsyncStorage,
    StyleSheet
} from 'react-native';
import {Button} from "../../components/buttons/Button";
import {customStyles} from "../../styles/styles";

export class DetailsView extends Component {
    styles = StyleSheet.create({
        long: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            height: 'auto',
            paddingTop: 10,
            paddingBottom: 10
        },
        label: StyleSheet.flatten([customStyles.label, {
            fontWeight: '900'
        }])
    });

    state = {
        item: this.props.navigation.getParam('item'),
        favourite: this.props.navigation.getParam('favourite')
    };

    acceptOffer = offer => {
        try {
            AsyncStorage.setItem('offer.' + offer._id, JSON.stringify(offer))
                .then(() => {
                    ToastAndroid.showWithGravity(
                        'Zgłoszenie zostało zapisane w obserwowancyh',
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER
                    );
                    this.props.navigation.navigate('OffersView');
                });
        } catch (error) {

        }
    };

    removeOffer = offer => {
        try {
            AsyncStorage.removeItem('offer.' + offer._id)
                .then(() => {
                    ToastAndroid.showWithGravity(
                        'Zgłoszenie zostało usunięte w obserwowancyh',
                        ToastAndroid.SHORT
                        , ToastAndroid.CENTER
                    );
                    this.props.navigation.navigate('OffersView');
                });
        } catch (error) {

        }
    };

    renderStatus = status => status ? 'Tak' : 'Nie';

    renderButton() {
        if (this.state.favourite) {
            return (
                <Button
                    label="Usuń ze śledzonych"
                    type="error"
                    onPress={() => this.removeOffer(this.state.item)}
                />
            )
        } else {
            return (
                <Button
                    label="Zapisz"
                    onPress={() => this.acceptOffer(this.state.item)}
                />
            )
        }
    }

    render() {
        return (
            <View style={[customStyles.container, customStyles.containerSpaceBetween]}>

                <ScrollView style={customStyles.list}>

                    <View style={customStyles.listElement}>
                        <Text style={this.styles.label}>Data wystawienia:</Text>
                        <Text style={customStyles.text}>{this.state.item.date}</Text>
                    </View>

                    <View style={customStyles.listElement}>
                        <Text style={this.styles.label}>Oferowana kwota netto:</Text>
                        <Text style={customStyles.text}>{this.state.item.value} PLN</Text>
                    </View>

                    <View style={customStyles.listElement}>
                        <Text style={this.styles.label}>Nr kontaktowy:</Text>
                        <Text style={customStyles.text}>{this.state.item.phone}</Text>
                    </View>

                    <View style={[customStyles.listElement, this.styles.long]}>
                        <Text style={this.styles.label}>Opis:</Text>
                        <Text
                            style={[customStyles.text, customStyles.textJustified]}>{this.state.item.description}</Text>
                    </View>

                    <View style={customStyles.listElement}>
                        <Text style={this.styles.label}>Praca w grupie:</Text>
                        <Text style={customStyles.text}>{this.renderStatus(this.state.item.groupWork)}</Text>
                    </View>

                    <View style={customStyles.listElement}>
                        <Text style={this.styles.label}>Jednorazowe zlecenie:</Text>
                        <Text style={customStyles.text}>{this.renderStatus(this.state.item.oneTime)}</Text>
                    </View>

                </ScrollView>

                {this.renderButton()}

            </View>
        )
    }
}