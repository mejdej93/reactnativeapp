import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Image} from 'react-native';
import {Button} from "../../components/buttons/Button";
import {customStyles} from "../../styles/styles";
import logo from './../../assets/logo.png';

export class LoginView extends Component {

    styles = StyleSheet.create({
        loginContainer: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        },
        logo: {
            display: 'flex',
            alignItems: 'center',
            padding: 20
        },
        loginForm: {
            padding: '20 20 0 20'
        },
        flexOne: {
            flex: 1
        },
        flexTwo: {
            marginBottom: 30
        },
        flexThree: {
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'center'
        }
    });

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        const imageStyle = {
            width: 150,
            height: 150
        };

        return (
            <View style={[customStyles.container, this.styles.loginContainer]}>

                <View style={this.styles.logo}>
                    <Image
                        accessibilityLabel="logo"
                        source={logo}
                        style={imageStyle}
                    />
                </View>

                <View style={this.styles.flexTwo}>
                    <Text style={customStyles.label}>Login</Text>
                    <TextInput style={customStyles.input}/>

                    <Text style={customStyles.label}>Hasło</Text>
                    <TextInput style={customStyles.input}/>
                </View>

                <View style={this.styles.flexThree}>
                    <Button
                        accessibilityLabel="login_button"
                        onPress={() => this.props.navigation.navigate('OffersView')}
                        label="Zaloguj"
                    />
                </View>
            </View>
        );
    }
}