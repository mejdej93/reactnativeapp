import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import {LoginView} from "./src/views/Login/Login.view";
import {DetailsView} from "./src/views/Details/Details.view";
import {OffersView} from "./src/views/Offers/Offers.view";
import {colors} from "./src/styles/styles";

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default App = createStackNavigator(
    {
        LoginView: {
            screen: LoginView,
            navigationOptions: {
                title: "Logowanie"
            }
        },
        OffersView: {
            screen: OffersView,
            navigationOptions: {
                title: "Oferty"
            }
        },
        DetailsView: {
            screen: DetailsView,
            navigationOptions: ({navigation}) => ({
                title: `${navigation.state.params.item.title}`,
            })
        }
    },
    {
        initialRouteName: 'LoginView',
        navigationOptions: ({navigation}) => ({
            headerStyle: {
                backgroundColor: colors.primary,
            },
            headerTitleStyle: {
                color: colors.fontColor,
            },
            headerBackTitleStyle: {
                color: colors.fontColor,
            },
            headerTintColor: colors.fontColor
        })
    }
);